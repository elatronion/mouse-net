#include <HGE/HGE_Core.h>
#include "mouse.h"

void PrintHelpMessage() {
	printf("If you want to host a mousenet server:\n\tmousenet host\nIf you want to connect to a mousenet server:\n\tmousenet connect <ip>\n");
}

char* ip_str;

int main(int argc, char **argv) {

	printf("argc: %d\n", argc);
	if(argc <= 1) {
		PrintHelpMessage();
		return 0;
	}

	printf("argv[1] : %s\n", argv[1]);

	if (strcmp(argv[1], "host") == 0) {
		printf("Woot Woot!\n");
		server_main();
	} else if(strcmp(argv[1], "connect") == 0) {
		if(argc < 3) {
			PrintHelpMessage();
			return 0;
		}

		size_t size_of_text = strlen(argv[2]);
		ip_str = malloc(size_of_text);
		for(int i = 0; i < size_of_text; i++) ip_str[i] = argv[2][i];
		client_main();
	} else {
		PrintHelpMessage();
	}

	return 0;
}

int server_main()
{
	hge_window window = { "Hyper Game Engine 1.0.0", 800, 600 };
	hgeInit(60, window, HGE_INIT_ECS | HGE_INIT_NETWORKING);

	// ECS //

	// Systems //
	hgeAddBaseSystems();
	hgeAddSystem(MouseMovementSyncSystem, 1, "Mouse");

	// Entities //
	// Camera
	hge_entity* camera_entity = hgeCreateEntity();
	hge_camera cam = {true, true, 1.f/6.f, hgeWindowWidth(), hgeWindowHeight(), (float)hgeWindowWidth() / (float)hgeWindowHeight(), 0.01f, 100.0f };
	hge_vec3 camera_position = {0, 0, 2};
	orientation_component camera_orientation = {0.0f, -90.0f, 0.0f};
	hgeAddComponent(camera_entity, hgeCreateComponent("Camera", &cam, sizeof(cam)));
	hgeAddComponent(camera_entity, hgeCreateComponent("Position", &camera_position, sizeof(camera_position)));
	hgeAddComponent(camera_entity, hgeCreateComponent("Orientation", &camera_orientation, sizeof(camera_orientation)));
	tag_component activecam_tag;
	hgeAddComponent(camera_entity, hgeCreateComponent("ActiveCamera", &activecam_tag, sizeof(activecam_tag)));

	// Mouse Receive
	hge_entity* mouse_entity = hgeCreateEntity();
	tag_component* mouse;
	hgeAddComponent(mouse_entity, hgeCreateComponent("Mouse", &mouse, sizeof(mouse)));

	XLibInit();

	hgeNetworkHost(7777, 32);

	// Start The Game/Engine
	hgeStart();

	return 0;
}

int client_main()
{
	hge_window window = { "Hyper Game Engine 1.0.0", 800, 600 };
	hgeInit(60, window, HGE_INIT_ECS | HGE_INIT_NETWORKING);

	// ECS //

	// Systems //
	hgeAddBaseSystems();
	hgeAddSystem(MouseSendSystem, 1, "Mouse");

	// Entities //
	// Camera
	hge_entity* camera_entity = hgeCreateEntity();
	hge_camera cam = {true, true, 1.f/6.f, hgeWindowWidth(), hgeWindowHeight(), (float)hgeWindowWidth() / (float)hgeWindowHeight(), 0.01f, 100.0f };
	hge_vec3 camera_position = {0, 0, 2};
	orientation_component camera_orientation = {0.0f, -90.0f, 0.0f};
	hgeAddComponent(camera_entity, hgeCreateComponent("Camera", &cam, sizeof(cam)));
	hgeAddComponent(camera_entity, hgeCreateComponent("Position", &camera_position, sizeof(camera_position)));
	hgeAddComponent(camera_entity, hgeCreateComponent("Orientation", &camera_orientation, sizeof(camera_orientation)));
	tag_component activecam_tag;
	hgeAddComponent(camera_entity, hgeCreateComponent("ActiveCamera", &activecam_tag, sizeof(activecam_tag)));

	// Mouse Send
	hge_entity* mouse_entity = hgeCreateEntity();
	tag_component* mouse;
	hgeAddComponent(mouse_entity, hgeCreateComponent("Mouse", &mouse, sizeof(mouse)));

	// Connect To IP
	hgeNetworkConnect(ip_str, 7777);

	free(ip_str);

	// Start The Game/Engine
	hgeStart();
	return 0;
}
