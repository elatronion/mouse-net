#ifndef MOUSE_MOVE_H
#define MOUSE_MOVE_H

#include <HGE/HGE_Core.h>

void XLibInit();
void SetMouseSendFrequency(float seconds);

void MouseSendSystem(hge_entity* entity, tag_component* mouse);
void MouseMovementSyncSystem(hge_entity* entity, tag_component* mouse);

#endif
