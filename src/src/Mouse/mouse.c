#include "mouse.h"

#define SCREEN_OFFSET_Y 780

#define EVENT_MOUSE_MOVE 	1
#define EVENT_MOUSE_CLICK 	2
#define EVENT_KEYBOARD_EVENT	3

float mouse_send_timer = 0;
float mouse_send_frequency = 1.f;

void SetMouseSendFrequency(float seconds) {
	mouse_send_frequency = seconds;
}

void SendKeyboardEvents() {
	for(int i = 0; i < HGE_KEY_LAST; i++) {
		if(hgeInputGetKeyDown(i)) {
			unsigned char data_to_send[3];
			data_to_send[0] = EVENT_KEYBOARD_EVENT;
       		data_to_send[1] = 0x1;
       		data_to_send[2] = i;
       		hgeNetworkSendPacket(&data_to_send[0], 3);
		} else if(hgeInputGetKeyUp(i)) {
			unsigned char data_to_send[3];
			data_to_send[0] = EVENT_KEYBOARD_EVENT;
       		data_to_send[1] = 0x0;
       		data_to_send[2] = i;
       		hgeNetworkSendPacket(&data_to_send[0], 3);
		}
	}
}

#ifdef __unix__
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>
#include <X11/extensions/XTest.h>

Display* dpy;
Window root_window;

void WarpMouse(int x, int y) {
	XWarpPointer(dpy, None, root_window, 0, 0, 0, 0, x, y);
	XSync(dpy, False);
	XFlush(dpy);
}

void SimulateLeftClickDown() {
	XTestFakeButtonEvent(dpy, 1, True, CurrentTime);
	XFlush(dpy);
}

void SimulateLeftClickUp() {
	XTestFakeButtonEvent(dpy, 1, False, CurrentTime);
	XFlush(dpy);
}

void SimulateKeyDown(unsigned int keycode) {
	unsigned int keycode_x = XKeysymToKeycode(dpy, keycode);
	XTestFakeKeyEvent(dpy, keycode_x, True, 0);
	XFlush(dpy);
}

void SimulateKeyUp(unsigned int keycode) {
	unsigned int keycode_x = XKeysymToKeycode(dpy, keycode);
	XTestFakeKeyEvent(dpy, keycode_x, False, 0);
	XFlush(dpy);
}

void XLibClean() {
	XCloseDisplay(dpy);
}

void XLibInit() {
	dpy = XOpenDisplay((char *) NULL);
	root_window = XRootWindow(dpy, 0);
	XSelectInput(dpy, root_window, KeyReleaseMask);
	WarpMouse(100, 1000);
	atexit(XLibClean);
}

#elif defined(_WIN32) || defined(WIN32)

void WarpMouse(int x, int y) {}
void SimulateLeftClickDown() {}
void SimulateLeftClickUp() {}
void SimulateKeyDown(unsigned int keycode) {}
void SimulateKeyUp(unsigned int keycode) {}
void XLibClean() {}
void XLibInit() {}

#endif

short unsigned int mouse_x;
short unsigned int mouse_y;

void MouseSendSystem(hge_entity* entity, tag_component* mouse) {
	if(hgeInputGetMouseDown(HGE_MOUSE_LEFT)) {
		unsigned char data_to_send[2];
		data_to_send[0] = EVENT_MOUSE_CLICK;
       	data_to_send[1] = 0x1;
       	hgeNetworkSendPacket(&data_to_send[0], 2);
       	return;
	}
	if(hgeInputGetMouseUp(HGE_MOUSE_LEFT)) {
		unsigned char data_to_send[2];
		data_to_send[0] = EVENT_MOUSE_CLICK;
       	data_to_send[1] = 0x0;
       	hgeNetworkSendPacket(&data_to_send[0], 2);
       	return;
	}

	SendKeyboardEvents();

	if(mouse_x == hgeInputGetMousePosition().x && mouse_y == hgeInputGetMousePosition().y) return;
	mouse_x = hgeInputGetMousePosition().x;
	mouse_y = hgeInputGetMousePosition().y;

        mouse_send_timer += hgeDeltaTime();
        if(mouse_send_timer >= mouse_send_frequency) {
        	mouse_send_timer = 0;
		uint8_t* mouse_x_ptr = (uint8_t*)&mouse_x;
		uint8_t* mouse_y_ptr = (uint8_t*)&mouse_y;

		unsigned char data_to_send[5];
		data_to_send[0] = EVENT_MOUSE_MOVE;
       	data_to_send[1] = mouse_x_ptr[0];
       	data_to_send[2] = mouse_x_ptr[1];
       	data_to_send[3] = mouse_y_ptr[0];
        	data_to_send[4] = mouse_y_ptr[1];
		hgeNetworkSendPacket(&data_to_send[0], 5);
	}
}

void MouseMovementSyncSystem(hge_entity* entity, tag_component* mouse) {

	hge_network_events* eventlist = hgeNetworkEventList();

	if(eventlist->num_messages > 0) {

		const char* recived_data = eventlist->messages[eventlist->num_messages-1].data;

		short unsigned int mouse_x_read;
		short unsigned int mouse_y_read;

		switch(recived_data[0]) {
			case EVENT_MOUSE_MOVE:
				mouse_x_read = (recived_data[1] & 0xff) | (recived_data[2] & 0xff) << 8;
				mouse_y_read = (recived_data[3] & 0xff) | (recived_data[4] & 0xff) << 8;

				// Warp Mouse To Received Position, currently disabled.
				// Warping the mouse does NOT work to simulate mouse motion. (tested with Minecraft and GTA:V)
				// Works great with GIMP
				if(0) WarpMouse(mouse_x_read, mouse_y_read + SCREEN_OFFSET_Y);
			break;
			case EVENT_MOUSE_CLICK:
				if(recived_data[1] == 0x0) 	SimulateLeftClickUp();
				else 				SimulateLeftClickDown();
			break;
			case EVENT_KEYBOARD_EVENT:
				if(recived_data[1] == 0x0) 	SimulateKeyUp(recived_data[2]);
				else 				SimulateKeyDown(recived_data[2]);
			break;
		}

		for(int i = 0; i < eventlist->num_messages; i++) free(eventlist->messages[i].data);
		eventlist->num_messages = 0;
	}

	// Red Button
	bool CLOSE_KEY = hgeInputGetKeyDown(HGE_KEY_F);
	if(CLOSE_KEY) {
		printf("Mouse Is Free!\n");
		hgeDestroyEntity(entity);
	}
}
